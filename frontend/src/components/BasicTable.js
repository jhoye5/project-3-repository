import React from "react";
import ReactDOM from "react-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, count, img, barcode) {
  return { name, count, img, barcode };
}

const rows = [
  createData("Product #1", 22, "img_link", "barcode_link"),
  createData("Product #2", 31, "img_link", "barcode_link"),
  createData("Product #3", 11, "img_link", "barcode_link"),
  createData("Product #4", 47, "img_link", "barcode_link"),
  createData("Product #5", 29, "img_link", "barcode_link"),
];

function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Product</TableCell>
            <TableCell align="right">Count</TableCell>
            <TableCell align="right">Image</TableCell>
            <TableCell align="right">Barcode</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell align="left">{row.name}</TableCell>
              <TableCell align="right">{row.count}</TableCell>
              <TableCell align="right">{row.img}</TableCell>
              <TableCell align="right">{row.barcode}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default BasicTable;